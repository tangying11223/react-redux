
import ReactDOM from 'react-dom';
import React,{Component} from 'react';
import { HashRouter , Switch, Route, NavLink} from 'react-router-dom';//BrowserRouter
import NoMatch from './Modules/NoMatch';
import routes from './router-config';
import Home from './Modules/Home';//./modules.index(模块配置)
import Headers from './Modules/Header';//./modules.index(模块配置)
import BlockList from './Modules/blockList';//./modules.index(模块配置)
import {Provider,connect} from 'react-redux';
import store from './store';



ReactDOM.render(
    <Provider store={store}>
            <HashRouter>
            <div>
            <Headers/>
                <hr/>
                <Switch>
                <Route path="/" component={Home} exact={true}/>

                    {routes.map((route,index)=>
                         <Route key={index} path={route.path} component={route.component}/>
                    )}
                  
                    <Route component={NoMatch}/>
                </Switch>
            </div>
            </HashRouter>
    </Provider>
,document.getElementById('roodt'))
// 这个在webpack.config.js   使用语法loader:"jsx-loader",