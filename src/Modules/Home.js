import React,{Component} from 'react';
import BlockList from './blockList';
import Footer from './Footer';
import {connect} from 'react-redux';
class Home extends Component{
    render(){
        const {count,lists} =this.props;
                console.log(this.props.lists);
        return(
            <div className="container marketing">
                <h3>我们的方法</h3>
                {count}
                <div className="row">
                    <div className="col-lg-4 w">
                    <img className="img-circle" src="/images/1.gif" alt="Generic placeholder image" width="140" height="140"/>
                    <h2>技术</h2>
                    <p>我们使用我们的iOs移动应用程序与我们的内部技术来连接您最近的服务供应商。我们是浪费的“超级”。没有更多的通过谷歌轻弹或担心，如果拿起你的货物的家伙是行货。</p>
                    <p><a className="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                    </div>
                    <div className="col-lg-4 w">
                    <img className="img-circle" src="/images/2.gif" alt="Generic placeholder image" width="140" height="140"/>
                    <h2>效率</h2>
                    <p>我们提供有保证的24小时送货服务，经常可以在同一天。加工？通过选择一个适合你的时间和日期安排一个旧冰箱的接机。没有更多的等待</p>
                    <p><a className="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                    </div>
                    <div className="col-lg-4 w">
                    <img className="img-circle" src="/images/3.gif" alt="Generic placeholder image" width="140" height="140"/>
                    <h2>可持续发展</h2>
                    <p>我们回收了94％的垃圾。我们提供可靠的可持续解决方案 去年伦敦发生的小费事件超过了35万，每年花费英国纳税人6亿英镑，同时也是不道德的。</p>
                    <p><a className="btn btn-default" href="#" role="button">View details &raquo;</a></p>
                    </div>
                </div>
                <BlockList/>
                <Footer/>
            </div>                   
        )
    }
}

const mapStateToProps=(state)=>{//方法 相当于store。getState。count
    console.log(state);
    
    return {
        count :state.count,
        lists :state.lists,
     

    }
}
export default connect(mapStateToProps)(Home)//不做任何操作 第二个参数不需要
