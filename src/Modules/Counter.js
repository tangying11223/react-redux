import React,{Component} from 'react';
import {Provider,connect} from 'react-redux';

import {increase,decrease,fetchList,addToCartAt} from '../actions/counter';//导入动作
// import index from '../../node_modules/_redux-thunk@2.2.0@redux-thunk';


 class Counter extends Component{
    componentWillMount(){
        this.props.fetchList()
    }
    changPage=(e)=>{
        // console.log(e.currentTarget.getAttribute('page') 取到自定义的属性);
        var page=e.currentTarget.getAttribute('page')
        const params={page:parseInt(page)}
        this.props.fetchList(params);
        
    }
    addCart(e){
        var idx=e.currentTarget.getAttribute('idx')
        var id=e.currentTarget.getAttribute('data-id')
        const addToCart=this.props.lists.subjects[idx];
        // console.log(id,idx,addToCart);
        this.props.addToCartAt(addToCart)
    }
    render(){
        // console.log(this.props);
        const {count,increase,count1,decrease,lists,fetchList,carts,addToCartAt}=this.props;
    // console.log(carts);
    
        if(!lists.subjects){
            return <div>数据请求中</div>
        }
        if(lists.subjects){
            var lst=[];
            var outputPages=[];
            var pages=Math.ceil(lists.total/lists.count);
            for (let i = 0; i <=pages; i++) {
                outputPages.push(
                        //   <li key={i}>{lists.subjects[i].title}</li>
                        //解析： (e)=>{this.changPage(e)=}
                           //事件的触发   创建一个自定义函数名为changPage 
                          <li className="active" key={i}><a href="javascript:void(0)" page={i} onClick={(e)=>{
                              this.changPage(e)
                          }}>{i} <span className="sr-only"></span></a></li>
                )
            }    
            lists.subjects.map((item,i)=>{
                return lst.push(
                    <li key={i}>{item.title}
                        <button className="btn btn-primary" 
                        data-id={item.id} idx={i}
                        onClick={(e)=>{this.addCart(e)}}>加入购物车</button>
                    </li>
                )
            })
                   
        }

        return(
            <div className="container">
                <span>count:{count}</span>
                <button onClick={increase} className="btn btn-primary">OWQ++</button>
                <button onClick={decrease} className="btn btn-primary">OWQ--</button>
                <span>count1:{count1}</span><br/>
                <ul>{lst}</ul>

                <nav aria-label="...">
                    <ul className="pagination">
                        {outputPages}
                    </ul>
                </nav>    
            </div>                   
        )
    }
}

const mapStateToProps=(state)=>{//方法 相当于store。getState。count
    return {
        count :state.count,
        count1 :state.count1,
        lists :state.lists,
        carts:state.carts
    }
}
export default connect(mapStateToProps,{increase,decrease,fetchList,addToCartAt})(Counter)
